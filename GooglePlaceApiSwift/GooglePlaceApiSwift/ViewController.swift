//
//  ViewController.swift
//  GooglePlaceApiSwift
//
//  Created by KMSOFT on 19/04/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker

class ViewController: UIViewController {
    
    var placesClient:GMSPlacesClient!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    let locationManager = CLLocationManager()
    let titleString = "MapViewController"
    override func viewDidLoad() {
        super.viewDidLoad()
        placesClient = GMSPlacesClient.shared()
        self.nameLabel.layer.cornerRadius = 5.0
        self.nameLabel.clipsToBounds = true
        self.addressLabel.layer.cornerRadius = 5.0
        self.addressLabel.clipsToBounds = true
        
        locationManager.requestAlwaysAuthorization()
        
        let next = UIBarButtonItem(title: "NEXT", style: .plain, target: self, action: #selector(NextControl))
        self.navigationItem.rightBarButtonItem = next
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = self.titleString
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.title = "BACK"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func NextControl(){
        let messageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlaceAutocompleteViewController") as! PlaceAutocompleteViewController
        self.navigationController?.pushViewController(messageViewController, animated: true)
    }
    
    @IBAction func getCurrentPlace(_ sender: UIButton) {
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            self.nameLabel.text = "No current place"
            self.addressLabel.text = ""
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    self.nameLabel.text = place.name
                    self.addressLabel.text = place.formattedAddress?.components(separatedBy: ", ")
                        .joined(separator: "\n")
                    print("Current Place name \(place.name) at likelihood \(placeLikelihoodList.likelihoods)")
                    print("Current Place address \(place.formattedAddress)")
                    print("Current Place attributions \(place.attributions)")
                    print("Current PlaceID \(place.placeID)")
                }
            }
        })
    }
    
    @IBAction func pickPlace(_ sender: UIButton) {
        let center = CLLocationCoordinate2D(latitude: 37.788204, longitude: -122.411937)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                self.nameLabel.text = place.name
                self.addressLabel.text = place.formattedAddress?.components(separatedBy: ", ")
                    .joined(separator: "\n")
            } else {
                self.nameLabel.text = "No place selected"
                self.addressLabel.text = ""
            }
        })
    }
}

