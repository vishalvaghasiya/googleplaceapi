//
//  PlaceAutocompleteViewController.swift
//  GooglePlaceApiSwift
//
//  Created by KMSOFT on 19/04/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class PlaceAutocompleteViewController: UIViewController , UISearchBarDelegate , LocateOnTheMap , GMSAutocompleteFetcherDelegate {
    /**
     * Called when an autocomplete request returns an error.
     * @param error the error that was received.
     */
    public func didFailAutocompleteWithError(_ error: Error) {
        print("\(error.localizedDescription)")
    }
    
    /**
     * Called when autocomplete predictions are available.
     * @param predictions an array of GMSAutocompletePrediction objects.
     */
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction!{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        //   self.searchResultsTable.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }
    let titleString = "PlaceAutocomplete"
    @IBOutlet var googleMapsContainer: UIView!
    
    var googleMapView:GMSMapView!
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let next = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchWithAddress))
        self.navigationItem.rightBarButtonItem = next
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = self.titleString
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.googleMapView = GMSMapView(frame: self.googleMapsContainer.frame)
        self.view.addSubview(self.googleMapView)
        
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.title = "BACK"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        DispatchQueue.main.async { () -> Void in
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position: position)
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
            self.googleMapView.camera = camera
            marker.title = "Address : \(title)"
            marker.map = self.googleMapView
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let placeClient = GMSPlacesClient()
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil)  {(results, error: Error?) -> Void in
            //            print("Error : \(error.debugDescription)")
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            for result in results! {
                
                if let result = result as? GMSAutocompletePrediction{
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
            self.searchResultController.reloadDataWithArray(self.resultsArray)
        }
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
    }
    
    func searchWithAddress(){
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
        self.present(searchController, animated: true, completion: nil)
    }
}
